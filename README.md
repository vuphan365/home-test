# Staff Management - Home-test


## Clone

```bash
git clone https://gitlab.com/vuphan365/home-test.git
cd home-test
npm install
```

## Start

```bash
npm run start
```

## Structure

```bash
├── src
     ├── __mocks__               # Define mock data
     ├── assets                  # Hold image, doc, ...
     ├── components              # Contain reused components, sub-components
     ├── pages                   # UI pages
     ├── utils                   # Contain supported-modules used in app
```

## Feature

- Display list of staffs with infinite-loading
- Create new staff
- Update/Delete existed staff
