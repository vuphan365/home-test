import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import HeaderNavbar from 'components/HeaderNavbar';
import Footer from 'components/Footer';
import StaffManagement from 'pages/StaffManagement';
import Nav2 from 'pages/Nav2';
import Nav3 from 'pages/Nav3';

export default function App() {
  return (
    <Router>
      <HeaderNavbar />
      <Switch>
        <Route path="/nav2">
          <Nav2 />
        </Route>
        <Route path="/nav3">
          <Nav3 />
        </Route>
        <Route path="/">
          <StaffManagement />
        </Route>
      </Switch>
      <Footer />
    </Router>
  )
}