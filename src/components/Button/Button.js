import styled from 'styled-components';

const Button = styled.button`
  ${props => props.type === "primary"
    ? "background-image: linear-gradient(rgb(10, 159, 191), rgb(62, 187, 147)); color: #ffffff; border: 0px;"
    : "background-color: #ffffff;color: #6e6e6e; border: 1px solid #6e6e6e;"}
  border-radius: 5px;
  padding: 8px 30px;
  cursor: pointer;
  display: flex;
  align-items: center;
  ${props => props.disabled && "pointer-events:none; opacity: 0.5;"}
  &:hover {
    opacity: 0.8
  }
  @media only screen and (max-width: 500px) {
    padding: 8px 20px;
  }
`;

export default Button;