import React from 'react';
import './Footer.scss';

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer__slogan">Extend human potential</div>
      <div className="footer__description">At Cinnamon AI, we are working to make a world where human creativy can flourish by using our
      AI technology to replace all the repetitive, mind-numbing tasks that take place today</div>
    </div>
  )
}

export default Footer;