import React from 'react';
import { Link } from 'react-router-dom';
import Logo from 'assets/logo.png';
import './HeaderNavbar.scss';

const HeaderNavbar = () => {
  return (
    <div className="header">
      <div className="header__logo">
        <Link to="/">
          <img src={Logo} width="195" height="34" />
        </Link>
      </div>
      <div className="header__nav">
        <div className="header__nav__element">
          <Link to="/">Home</Link>
        </div>
        <div className="header__nav__element">
          <Link to="/nav2">Requests</Link>
        </div>
        <div className="header__nav__element">
          <Link to="/nav3">My Page</Link>
        </div>
      </div>
    </div>
  )
};

export default HeaderNavbar;