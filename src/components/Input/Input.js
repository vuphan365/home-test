import styled from 'styled-components';

const Input = styled.input`
  padding: 5px;
  margin: 2px;
  background: #ffffff;
  outline: 0px;
  border: 1px solid #ced4da;
  border-radius: 3px;
  color: #495057;
  font-size: 15px;
  width: ${props => props.width || '250px'};
  &:focus {
    border-color: #80bdff;
    
    box-shadow: 0 0 0 0.2rem rgba(0,123,255,.25);
  }
`;

export default Input;