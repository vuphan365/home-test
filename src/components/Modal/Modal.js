import React from 'react';
import closeIcon from 'assets/close.svg'
import './Modal.scss';
class Modal extends React.Component {
  static Header = ({ children, onClose }) => (
    <div className="modal-comp__dialog__header">
      {children}
      <div className="modal-comp__dialog__header__close-icon" onClick={onClose}>
        <img src={closeIcon} />
      </div>
    </div>
  )

  static Content = ({ children }) => (
    <div className="modal-comp__dialog__content">
      {children}
    </div>
  )

  static Footer = ({ children, buttonGroup }) => (
    <div className="modal-comp__dialog__footer">
      <div>{children}</div>
      {buttonGroup.length > 0 && (
        <div className="modal-comp__dialog__footer__button">
          {buttonGroup}
        </div>
      )}
    </div>
  )
  render() {
    const { open } = this.props;
    return (
      <div className={`modal-comp ${open ? "show" : ""}`} >
        <div className="modal-comp__layout" />
        <div className="modal-comp__dialog">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Modal;