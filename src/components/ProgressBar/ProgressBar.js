import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  height: 11px;
  border-radius: 5px;
  border: 1px solid #3c3c3c;
  cursor: pointer;
  position: relative;
  .progress-popup {
    display: none;
    position: absolute;
    top: -1.5px;
    right: 40%;
    font-size: 13px;
  }
  &:hover { 
    .progress-popup {
      display: block;
    }
  }
  @media only screen and (max-width: 500px) {
    .progress-popup {
      right: 10%
    }
  }
`
const Insider = styled.div`
  height: 11px;
  border-radius: ${props => props.value === 1 ? '5px' : '5px 0px 0px 5px'};
  background-image: linear-gradient(rgb(10, 159, 191), rgb(62, 187, 147));
  width: ${props => props.value * 100}%;
`;

const ProgressBar = ({ value }) => (
  <Wrapper>
    <div className="progress-popup">{value * 100} % </div>
    <Insider value={value} />
  </Wrapper>
)
export default ProgressBar;