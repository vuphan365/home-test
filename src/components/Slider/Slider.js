import styled from 'styled-components';

const Slider = styled.input.attrs((props) => ({
  type: "range",
  min: props.min,
  max: props.max,
  step: (props.max - props.min) / 10
}))`
  width: ${props => props.width || '250px'};
`;

export default Slider;