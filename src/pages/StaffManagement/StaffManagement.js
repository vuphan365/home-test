import React from 'react';
import { getStaffList } from 'utils/api';
import { Button } from 'components';
import AddButton from 'assets/add.svg';
import LoadingIcon from 'assets/loading.gif';
import StaffTable from './StaffTable';
import UpdateStaff from './UpdateStaff';
import './StaffManagement.scss';

const options = {
  rootMargin: '0px',
  threshold: 0.1,
};
let observer;

const generateDefaultStaff = () => ({
  user_name: "",
  full_name: "",
  title: "",
  resource_capacity: 0,
  status: false
})
class StaffManagement extends React.Component {
  loadTarget = React.createRef();

  state = {
    staffList: [],
    pagination: {
      limit: 5,
      page: 0,
    },
    isLoading: false,
    isOpen: false,
    staff: generateDefaultStaff()
  }

  componentDidMount() {
    this.handleFetchStaffList();
  }

  componentDidUpdate() {
    if (!observer && this.loadTarget.current) {
      observer = new IntersectionObserver(this.onObserver, options);
      observer.observe(this.loadTarget.current)
    }
  }

  componentWillUnmount() {
    observer.unobserve(this.loadTarget.current)
  }

  onObserver = (entries) => {
    if (entries) {
      const listIntersecting = entries.map((entry) => entry.isIntersecting);
      const { pagination: { totalPages, page }, isLoading } = this.state;
			if (listIntersecting.indexOf(true) !== -1 && (!totalPages || totalPages > page) && !isLoading) {
        this.handleFetchStaffList();
			}
		}
  }

  forceReobserver = () => {
    if (observer && this.loadTarget.current) {
      observer.unobserve(this.loadTarget.current)
      observer.observe(this.loadTarget.current)
    }
  }

  handleFetchStaffList = async () => {
    this.setState({ isLoading: true });
    const { pagination } = this.state;
    const { staffs, pagination: pages } = await getStaffList(pagination.limit, pagination.page + 1);
    this.setState(prevState => ({
      staffList: [...prevState.staffList, ...staffs],
      pagination: pages,
      isLoading: false 
     }));
     this.forceReobserver();
  }

  handleCloseModal = () => {
    this.setState({ isOpen: false, staff: generateDefaultStaff() });
  }

  handleOpenAddNewStaff = () => {
    this.setState({ isOpen: true, staff: generateDefaultStaff() });
  }

  handleUpdateStaff = (staff) => {
    this.setState({ isOpen: true, staff });
  }

  handleDeleteStaff = (staff) => {
    this.setState(prevState => ({
      staffList: prevState.staffList.filter(st => st.id !== staff.id),
    }));
  }

  handleSaveStaff = (staff) => {
    // Update locally
    if (staff.id) {
      this.setState(prevState => ({
        staffList: prevState.staffList.map(st => st.id === staff.id ? staff : st),
      }));
    } else {
      this.setState(prevState => ({
        staffList: [
          { ...staff, id: Math.random() },
          ...prevState.staffList
        ],
      }));
    }
    this.handleCloseModal();
  }

  render() {
    const { staffList, isLoading, staff, isOpen } = this.state;
    return (
      <div className="staff-mng">
        <UpdateStaff
          isOpen={isOpen}
          staff={staff}
          handleCloseModal={this.handleCloseModal}
          handleSaveStaff={this.handleSaveStaff}
        />
        <div className="staff-mng__header">
          <div className="staff-mng__header__name">Staff Management</div>
          <Button type="primary" disabled={isLoading} onClick={this.handleOpenAddNewStaff}>
            <img src={AddButton} className="staff-mng__header__add-icon" />
            New staff
          </Button>
        </div>
        <StaffTable
          staffList={staffList}
          handleUpdateStaff={this.handleUpdateStaff}
          handleDeleteStaff={this.handleDeleteStaff}
        />
        <div className="staff-mng__loading">
          <div ref={this.loadTarget} className="staff-mng__loading__load-target"></div>
          <div className="staff-mng__loading__load-icon">
            {isLoading && <img src={LoadingIcon} />}
          </div>
        </div>
      </div>
    )
  }
}

export default StaffManagement;