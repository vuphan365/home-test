import React from 'react';
import { ProgressBar } from 'components';
import './StaffTable.scss';

const StaffTable = ({ staffList, handleUpdateStaff, handleDeleteStaff }) => {
  return (
    <div className="staff-tb">
      <table cellSpacing="0">
        <thead>
          <tr>
            <th>Username</th>
            <th>Full Name</th>
            <th>Title</th>
            <th>Resource Capacity</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {staffList.map((staff) => (
            <tr key={staff.id}>
              <td>{staff.user_name}</td>
              <td>{staff.full_name}</td>
              <td>{staff.title}</td>
              <td><ProgressBar value={staff.resource_capacity}/></td>
              <td>{staff.status ? 'Active' : 'Deactive'}</td>
              <td>
                <div className="staff-tb__button-group">
                  <div onClick={() => handleUpdateStaff(staff)} className="staff-tb__button-group__edit">Edit</div> |
                  <div onClick={() => handleDeleteStaff(staff)} className="staff-tb__button-group__delete">Delete</div>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
};

export default StaffTable;