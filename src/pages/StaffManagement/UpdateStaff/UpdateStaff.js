import React, { useState, useEffect } from 'react';
import { Button, Modal, Input, Slider } from 'components';
import './UpdateStaff.scss';

const FORM_VALUES = {
  USER_NAME: 'user_name',
  FULL_NAME: 'full_name',
  TITLE: 'title',
  RESOURCE_CAPACITY: 'resource_capacity',
  STATUS: 'status'
}

const UpdateStaff = ({
  handleCloseModal,
  staff,
  isOpen,
  handleSaveStaff
}) => {
  const [formValues, setFormValues] = useState({
    user_name: "",
    full_name: "",
    title: "",
    resource_capacity: 0,
    status: false
  });

  useEffect(() => {
    setFormValues(staff);
  }, [staff])

  const onChangeInput = (evt) => {
    if (evt.target.name === FORM_VALUES.STATUS) {
      setFormValues({
        ...formValues,
        [evt.target.name]: evt.target.value ? 1 : 0
      })
    } else {
      setFormValues({
        ...formValues,
        [evt.target.name]: evt.target.value
      });
    }
  }

  const onChangeCheckbox = (evt) => {
    setFormValues({
      ...formValues,
      [evt.target.name]: evt.target.checked
    })
  }

  const isDisableSave = () => {
    return !formValues[FORM_VALUES.USER_NAME] || !formValues[FORM_VALUES.FULL_NAME] || !formValues[FORM_VALUES.TITLE]
  }
  
  const onSave = () => {
    handleSaveStaff({
      id: staff.id,
      ...formValues,
      resource_capacity: Number(formValues[FORM_VALUES.RESOURCE_CAPACITY]),
    });
  }

  return (
    <Modal open={isOpen}>
      <Modal.Header onClose={handleCloseModal}>
        {staff.id ? "Update staff" : "Add new staff"}
      </Modal.Header>
      <Modal.Content>
        <div className="staff-upt__form">
          <div className="staff-upt__form-item">
            <div className="staff-upt__form-item__label">Username</div>
            <Input
              name={FORM_VALUES.USER_NAME}
              value={formValues[FORM_VALUES.USER_NAME]}
              onChange={onChangeInput}
              placeholder="Enter username"
            />
          </div>
          <div className="staff-upt__form-item">
            <div className="staff-upt__form-item__label">Full name</div>
            <Input
              name={FORM_VALUES.FULL_NAME}
              value={formValues[FORM_VALUES.FULL_NAME]}
              onChange={onChangeInput}
              placeholder="Enter full name"
            />
          </div>
          <div className="staff-upt__form-item">
            <div className="staff-upt__form-item__label">Title</div>
            <Input
              name={FORM_VALUES.TITLE}
              value={formValues[FORM_VALUES.TITLE]}
              onChange={onChangeInput}
              width="100px"
              placeholder="Enter title"
            />
          </div>
          <div className="staff-upt__form-item">
            <div className="staff-upt__form-item__label">Resource Capacity</div>
            <Slider
              name={FORM_VALUES.RESOURCE_CAPACITY}
              value={formValues[FORM_VALUES.RESOURCE_CAPACITY]}
              onChange={onChangeInput}
              min={0}
              max={1}
            />
          </div>
          <div className="staff-upt__form-item">
            <div className="staff-upt__form-item__label">Status</div>
            <input
              type="checkbox"
              className="staff-upt__form-item__checkbox"
              value={formValues[FORM_VALUES.STATUS]}
              name={FORM_VALUES.STATUS}
              onChange={onChangeCheckbox}
            /> Active
          </div>
        </div>
      </Modal.Content>
      <Modal.Footer
        buttonGroup={[
          <Button key="save" disabled={isDisableSave()} type="primary" onClick={onSave}>Save</Button>,
          <Button key="delete" onClick={handleCloseModal}>Close</Button>
        ]}
      />
    </Modal>
  )
}

export default UpdateStaff;