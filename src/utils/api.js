import staffMockData from '__mocks__/staff.json';

const length = staffMockData.length;
let timer = null;
export const getStaffList = (limit, page) => {
  const offset = (page - 1) * limit;
  const lastOffset = page * limit;
  const totalPages = Math.ceil(length / limit);
  const data = staffMockData.slice(Math.min(length, offset), Math.min(length, lastOffset));
  clearTimeout(timer);
  return new Promise((resolve) => {
    timer = setTimeout(() => {
      resolve({
        staffs: data,
        pagination: {
          limit,
          page,
          totalPages
        }
      })
    }, 500)
  })
}